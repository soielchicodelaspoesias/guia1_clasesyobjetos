#!/usr/bin/env python3
# -*- coding:utf-8 -*-

class biblioteca():
    def __init__(self, inventario):
        self.inventario = inventario
    # Agrego los libros a un diccionario, el titulo es la key y una lista con los demas datos es el value
    def agregar_libros(self, inventario, datos):
        titulo = input("ingrese el nombre del libro: ")
        autor = input("ingrese el autor del libro: ")
        ejem = input("ingrese numero de ejemplares: ")
        ejem_prestados = input("ingrese numero de ejemplares prestados: ")
        datos.append("autor: " + autor)
        datos.append("numero de ejemplares: " + ejem)
        datos.append("ejemplares prestados: " + ejem_prestados)
        inventario[titulo] = datos
    # Imprimo el diccionario con todos los libros agregados
    def print_inventrario(self, inventario):
        print("INVENTARIO DE LA BIBLIOTECA")
        print(inventario)

if __name__ == '__main__':
    inventario = {}
    datos = []
    biblioteca2 = biblioteca(inventario)
    a = 1
    # Voy agregando libros hasta que se aburra
    for i in range(a):
        biblioteca2.agregar_libros(inventario, datos)
        s = input("Ingrese una s si desea seguir agregando libros sino cualquier otra letra: ")
        if s == "s":
            a += 1
        else:
            break
    biblioteca2.print_inventrario(inventario)
