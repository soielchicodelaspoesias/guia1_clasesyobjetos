#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# Imprime el numero final, como es una lista la sumo a un texto para que salga el numero todo junto
def imprimir(numero6):
    texto = ""
    for i in range(4):
        texto+= str(numero6[i])
    print("El numero final es: ",texto)


class incriptador():
    def __init__(self, prim, segu, ter, cuar):
        self.prim = prim
        self.segu = segu
        self.ter = ter
        self.cuar = cuar
    def primera_parte(self, prim, segu, ter, cuar, numero2):
        numero3 = []
        # Voy sumando el numero y sus posiciones anterires, cada nuevo numero se guarda en una nueva lista
        for i in range(4):
            if i == 0:
                numero3.append(numero2[i])
            if i == 1:
                numero3.append(numero2[i] + numero2[i - 1])
            if i == 2:
                numero3.append(numero2[i] + numero2[i - 1] + numero2[i - 2])
            if i == 3:
                numero3.append(sum(numero2))
            # Si la suma es > 10 solo obtengo el ultimo digito y le sumo 7 a todos los numeros
            numero3[i] = ((numero3[i]%10) + 7)
        return numero3
    def segunda_parte(self, numero4):
        nuemero5 = []
        # Cambio el uno por el 3 y el dos por el 4
        unoydos = numero4[:2:-1]
        tresycuatro = numero4[2::-1]
        numero5 = tresycuatro + unoydos
        return numero5

if __name__ == '__main__':
    # Ingresa el numero y lo divide en 4 diferentes variables
    numero1 = int(input("Numero a incriptar: "))
    prim = int(numero1/1000)
    segu = int((numero1%1000)/100)
    ter = int((numero1%100)/10)
    cuar = numero1%10
    # Los agrego a una lista
    numero2 = [prim, segu, ter, cuar]
    incriptacion = incriptador(prim, segu, ter, cuar)
    numero4 = incriptacion.primera_parte(prim, segu, ter, cuar, numero2)
    numero6 = incriptacion.segunda_parte(numero4)
    imprimir(numero6)