#!/usr/bin/env python3
# -*- coding:utf-8 -*-


class friend_rajao():
    def __init__(self, faltante):
        self.faltante = faltante
    # Imprime el dinero que se le pasara a pepe
    def pagar_a_pepe(self, faltante):
        print("Se le pasara : $", faltante, "a pepe")


class pepe_friends():
    def __init__(self, saldo, cuentafinal, n_amigos, pagar):
        self.saldo = saldo
        self.cuenta_final = cuentafinal
        self.n_amigos = n_amigos
        self.pagar = pagar

    # imprime la boleta
    def boleta(self, cuenta, cuentafinal):
        print("Gasto en insumos: $", cuenta)
        print("IVA (19%): $", int((cuenta * 19) / 100))
        print("Propina (10%): $", int((cuenta * 10) / 100))
        print("Total: $", cuentafinal)


    def pepe_pagar(self, saldo, pagar, faltante, amigo_rajao):
        # si le falta dinero a pepe le pide al amigo rajado
        print("pepe debe pagar: $", pagar)
        if saldo < pagar:
            print("pepe no tiene sufiente dinero pero un amigo le pasara")
            amigo_rajao.pagar_a_pepe(faltante)
        else:
            print("Pepe pago y su vuelto es: $", saldo - pagar)
            oldsaldo = saldo
            saldo = oldsaldo - pagar


if __name__ == '__main__':
    saldo = int(input("Ingrese el saldo de pepe: $"))
    cuenta = int(input("Ingrese lo gastado en consumos: $"))
    cuentafinal = cuenta + (int((cuenta * 19) / 100)) + (int((cuenta * 10) / 100))
    n_amigos = int(input("Numero de amigos que acompañan a pepe: "))
    pagar = int(cuentafinal / (n_amigos + 1))
    faltante = pagar - saldo
    amigo_rajao = friend_rajao(faltante)
    pepe_amigos = pepe_friends(saldo, cuentafinal, n_amigos, pagar)
    pepe_amigos.boleta(cuenta, cuentafinal)
    pepe_amigos.pepe_pagar(saldo, pagar, faltante, amigo_rajao)

