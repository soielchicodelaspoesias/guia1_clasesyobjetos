#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# Declaracion del objeto
class tarjeta():
    def __init__(self, nombre, saldo):
        self.nombre = nombre
        self.saldo = saldo

    # Imprimir los datos de la cuenta
    def print_cuenta(self, nombre, saldo):
        print("DATOS DE LA CUENTA")
        print("Nombre: ", nombre)
        print("Saldo: $", saldo)

    # verificat que la compra no tiene valor nagativo y es menor al saldo
    def compra(self, saldo, pagado):
        if pagado > saldo and pagado > 0:
            print("Lo sentimos no tiene saldo suficiente")
        # Resta el valor comprado al saldo actual e imprime el nuevo saldo
        else:
            oldsaldo = saldo
            saldo = oldsaldo - pagado
            print("Su nuevo saldo es: $", saldo)


if __name__ == '__main__':
    nombre = input("Ingrese su nombre: ")
    saldo = int(input("Ingrese su saldo: "))
    tarjeta_credito = tarjeta(nombre, saldo)
    tarjeta_credito.print_cuenta(nombre, saldo)
    i = input("Indique si realizo una transaccion (s/n): ")
    if i == "s":
        pagado = int(input("Ingrese el monto de la compra: "))
        tarjeta_credito.compra(saldo,pagado)
