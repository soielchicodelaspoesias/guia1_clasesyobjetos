#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import random


def numero_cuenta():
    # Genero aleatoriamente el numero de la cuenta
    a = random.randrange(1000, 9999)
    b = random.randrange(1000, 9999)
    c = random.randrange(1000, 9999)
    d = random.randrange(1000, 9999)
    print("Numero de la tarjeta: ")
    print(a, b, c, d)


def menu(tarjeta_credito, nombre, saldo):
    print("MENU")
    print("1. Datos de tarjeta")
    print("2. Realizar tranferencia")
    print("3. Recibir pago")
    print("4. Salir")
    menu2 = int(input("Selecciones una accion: "))
    if menu2 == 1:
        tarjeta_credito.print_cuenta(nombre, saldo)
        menu(tarjeta_credito, nombre, saldo)
    if menu2 == 2:
        destino = input("Ingrese el nombre de la persona a la cual desea tranferir: ")
        pagado = int(input("Ingrese el monto a tranferir: "))
        tarjeta_credito.tranferencia(saldo, pagado, destino)
    if menu2 == 3:
        donador = input("Ingrese el nombre de la persona que le va a tranferir: ")
        monto = int(input("ingrese el monto a recibir: "))
        tarjeta_credito.recibir(donador, monto, saldo)
    if menu2 == 4:
        exit()


class tarjeta():


    def __init__(self, nombre, saldo):
        self.nombre = nombre
        self.saldo = saldo


    def print_cuenta(self,nombre, saldo):
        print("DATOS DE LA CUENTA")
        print("Nombre: ", nombre)
        numero_cuenta()
        print("Saldo: $", saldo)

    # verificat que la tranferencia no tiene valor nagativo y es menor al saldo
    def tranferencia(self, saldo, pagado, destino):
        if pagado > saldo and pagado > 0:
            print("Lo sentimos no tiene saldo suficiente")
            menu(tarjeta_credito, nombre, saldo)
        # Resta el valor tranferido al saldo actual e imprime el nuevo saldo
        else:
            oldsaldo = saldo
            saldo = oldsaldo - pagado
            print("Transferencia exitosa a: ", destino)
            print("Su nuevo saldo es: $", saldo)
            menu(tarjeta_credito, nombre, saldo)

    # Verifica que el monto recibido no es negativo y sino lo suma al saldo
    def recibir(self, donador, monto, saldo):
        if monto < 0:
            print("Lo sentimos ha ocurrido un error")
            menu(tarjeta_credito, nombre, saldo)
        else:
            print("Cobro realizado con exito a: ", donador, "por: $", monto)
            oldsaldo = saldo
            saldo = oldsaldo + monto
            menu(tarjeta_credito, nombre, saldo)
            print("Su nuevo saldo es: $", saldo)



if __name__ == '__main__':
    nombre = input("Ingrese su nombre: ")
    saldo = int(input("Ingrese su saldo: "))
    tarjeta_credito = tarjeta(nombre, saldo)
    menu(tarjeta_credito, nombre, saldo)


